#!/usr/bin/env python3

import os
import wx
import wx.adv
import subprocess
from appdirs import user_config_dir
import configparser
from pathlib import Path
from simple_ssh.logo import logo
from simple_ssh.SimpleSSHSettingsFrame import SimpleSSHSettingsFrame
from simple_ssh.gui import SettingsPanel, About

config_dir = user_config_dir('simplessh', 'indeed')
Path(config_dir).mkdir(parents=True, exist_ok=True)
path = Path(f'{config_dir}/simplessh.ini')

TRAY_TOOLTIP = 'Simple SSH'

def create_menu_item(menu, label, func):
    item = wx.MenuItem(menu, -1, label)
    menu.Bind(wx.EVT_MENU, func, id=item.GetId())
    menu.Append(item)
    return item

class TaskBarIcon(wx.adv.TaskBarIcon):
    def __init__(self,frame):
        wx.adv.TaskBarIcon.__init__(self)
        self.about = About(None)
        self.config = configparser.ConfigParser()
        self.config.read(path.absolute())
        self.frm = None
        self.conf_panels = {}
        self.servers_to_remove = []
        self.myapp_frame = frame
        self.SetIcon(logo.Icon, TRAY_TOOLTIP)
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DOWN, self.open_ssh_menu)

    def CreatePopupMenu(self):
        menu = wx.Menu()
        create_menu_item(menu, 'Settings', self.on_settings)
        menu.AppendSeparator()
        create_menu_item(menu, 'About', self.on_about)
        menu.AppendSeparator()
        create_menu_item(menu, 'Exit', self.on_exit)
        return menu

    def createSshMenu(self):
        menu = wx.Menu()
        for server in self.config.sections():
            if server == 'GLOBAL':
                continue
            create_menu_item(menu, server, self.connect_ssh(self.config[server]))
        #menu.AppendSeparator()
        return menu

    def open_ssh_menu(self, event):
        menu = self.createSshMenu()
        self.PopupMenu(menu)

    def on_settings(self, event):
        self.frm = SimpleSSHSettingsFrame(None, self)
        self.restore_settings()
        # self.frm.Layout()
        # self.frm.Fit()
        self.frm.Show()
        self.frm.general_panel.Fit()
        # self.frm.general_panel.Layout()

    def on_about(self, event):
        self.about.Show()
    
    def close_settings(self):
        if not self.frm:
            return
        self.frm.Close()
        self.frm = None
    
    def restore_settings(self):
        assert self.frm

        for server in self.config.sections():
            if server == 'GLOBAL':
                self.frm.m_console.SetValue(self.config['GLOBAL']['console'])
            else:
                self.add_settings_panel(server,
                    self.config[server]['hostname'],
                    self.config[server]['port'],
                    self.config[server]['username'],
                    self.config[server].get('password', ''),
                )
        self.frm.Fit()
    
    def add_settings_panel(self, server, hostname=None, port=None, username=None, password=None):
        panel = SettingsPanel(self.frm.m_notebook)
        if hostname:
            panel.txt_hostname.SetValue(hostname)
        if port:
            panel.txt_port.SetValue(port)
        if username:
            panel.txt_username.SetValue(username)
        if password:
            panel.txt_password.SetValue(password)
        page_cnt = self.frm.m_notebook.PageCount
        self.frm.m_notebook.InsertPage(page_cnt, panel, server, False)
        self.conf_panels[server] = panel
        return page_cnt

    def save_config(self):
        assert self.frm

        for server in self.servers_to_remove:
            del self.conf_panels[server]
            self.config.remove_section(server)
        self.servers_to_remove = []

        self.config['GLOBAL'] = {
            'console': self.frm.m_console.GetValue()
        }
        for server, panel in self.conf_panels.items():
            self.config[server] = {
                'hostname': panel.txt_hostname.GetValue(),
                'port': panel.txt_port.Value,
                'username': panel.txt_username.Value,
                'password': panel.txt_password.GetValue(),
            }
        with path.open('w') as f:
            self.config.write(f)
        self.close_settings()

    def connect_ssh(self, conf):
        def _connect_ssh(event):
            console = self.config['GLOBAL']['console']
            ssh_cmd = f"ssh -l {conf['username']} -p {conf['port']} {conf['hostname']}"
            shell = False
            if console == 'konsole':
                console_cmd = ["konsole", "-e", ssh_cmd]
            elif console == 'mate-terminal':
                console_cmd = ["mate-terminal", "-e", ssh_cmd]
            elif console == 'gnome-terminal':
                console_cmd = ["gnome-terminal", "--", "bash", "-c", ssh_cmd]
            elif console == 'xterm':
                console_cmd = ["xterm", "-e", ssh_cmd]
            elif console == 'alacritty':
                console_cmd = ["alacritty", "-e", ssh_cmd]
            elif console == 'power shell':
                console_cmd = ["start", "powershell", "-Command", ssh_cmd]
                shell = True
            else:
                print(f'TODO: Show warning about unsupported console here: {console}')
                console_cmd = ["xterm", "-e", ssh_cmd]
            subprocess.Popen(console_cmd, shell=shell)
        return _connect_ssh

    def on_exit(self, event):
        self.close_settings()
        self.about.Destroy()
        self.myapp_frame.Close()

class SimpleSSH(wx.Frame):

    #----------------------------------------------------------------------
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "", size=(1,1))
        panel = wx.Panel(self)
        self.myapp = TaskBarIcon(self)
        self.Bind(wx.EVT_CLOSE, self.onClose)

    #----------------------------------------------------------------------
    def onClose(self, evt):
        """
        Destroy the taskbar icon and the frame
        """
        self.myapp.RemoveIcon()
        self.myapp.Destroy()
        self.Destroy()
