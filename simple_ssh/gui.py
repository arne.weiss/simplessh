# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.adv

###########################################################################
## Class SettingsFrame
###########################################################################

class SettingsFrame ( wx.Frame ):

    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Simple SSH - Settings", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        bSizer5 = wx.BoxSizer( wx.VERTICAL )

        self.m_notebook = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.general_panel = wx.Panel( self.m_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

        m_consoleChoices = [ u"alacritty", u"konsole", u"mate-terminal", u"gnome-terminal", u"xterm", u"power shell" ]
        self.m_console = wx.ComboBox( self.general_panel, wx.ID_ANY, u"xterm", wx.DefaultPosition, wx.DefaultSize, m_consoleChoices, wx.CB_READONLY )
        self.m_console.SetSelection( 3 )
        bSizer3.Add( self.m_console, 1, wx.ALL, 5 )


        self.general_panel.SetSizer( bSizer3 )
        self.general_panel.Layout()
        bSizer3.Fit( self.general_panel )
        self.m_notebook.AddPage( self.general_panel, u"General", False )

        bSizer5.Add( self.m_notebook, 1, wx.ALL|wx.EXPAND, 5 )

        bSizer52 = wx.BoxSizer( wx.HORIZONTAL )

        self.btn_add = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

        self.btn_add.SetBitmap( wx.Bitmap( u"../simple_ssh/plus.png", wx.BITMAP_TYPE_ANY ) )
        bSizer52.Add( self.btn_add, 0, wx.ALL, 5 )

        self.btn_remove = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

        self.btn_remove.SetBitmap( wx.Bitmap( u"../simple_ssh/minus.png", wx.BITMAP_TYPE_ANY ) )
        self.btn_remove.Enable( False )

        bSizer52.Add( self.btn_remove, 0, wx.ALL, 5 )


        bSizer52.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.btn_save = wx.Button( self, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer52.Add( self.btn_save, 0, wx.ALL, 5 )

        self.btn_cancel = wx.Button( self, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer52.Add( self.btn_cancel, 0, wx.ALL, 5 )


        bSizer5.Add( bSizer52, 0, wx.BOTTOM|wx.EXPAND|wx.RIGHT, 5 )


        self.SetSizer( bSizer5 )
        self.Layout()
        bSizer5.Fit( self )

        self.Centre( wx.BOTH )

        # Connect Events
        self.m_notebook.Bind( wx.EVT_NOTEBOOK_PAGE_CHANGED, self.tab_changed )
        self.btn_add.Bind( wx.EVT_BUTTON, self.add_config )
        self.btn_remove.Bind( wx.EVT_BUTTON, self.remove_config )
        self.btn_save.Bind( wx.EVT_BUTTON, self.save_settings )
        self.btn_cancel.Bind( wx.EVT_BUTTON, self.close_settings )

    def __del__( self ):
        pass


    # Virtual event handlers, overide them in your derived class
    def tab_changed( self, event ):
        event.Skip()

    def add_config( self, event ):
        event.Skip()

    def remove_config( self, event ):
        event.Skip()

    def save_settings( self, event ):
        event.Skip()

    def close_settings( self, event ):
        event.Skip()


###########################################################################
## Class SettingsPanel
###########################################################################

class SettingsPanel ( wx.Panel ):

    def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
        wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

        fgSizer1 = wx.FlexGridSizer( 0, 2, 0, 0 )
        fgSizer1.AddGrowableCol( 1 )
        fgSizer1.SetFlexibleDirection( wx.HORIZONTAL )
        fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.lbl_hostname = wx.StaticText( self, wx.ID_ANY, u"Hostname", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.lbl_hostname.Wrap( -1 )

        fgSizer1.Add( self.lbl_hostname, 0, wx.ALL, 8 )

        bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

        self.txt_hostname = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.txt_hostname.SetMinSize( wx.Size( 180,-1 ) )

        bSizer6.Add( self.txt_hostname, 1, wx.ALL|wx.EXPAND, 5 )

        self.lbl_port = wx.StaticText( self, wx.ID_ANY, u"Port", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.lbl_port.Wrap( -1 )

        bSizer6.Add( self.lbl_port, 0, wx.ALL, 8 )

        self.txt_port = wx.TextCtrl( self, wx.ID_ANY, u"22", wx.DefaultPosition, wx.Size( 35,-1 ), 0 )
        self.txt_port.SetMaxLength( 2 )
        bSizer6.Add( self.txt_port, 0, wx.ALL, 5 )


        fgSizer1.Add( bSizer6, 1, wx.EXPAND, 5 )

        self.lbl_username = wx.StaticText( self, wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.lbl_username.Wrap( -1 )

        fgSizer1.Add( self.lbl_username, 0, wx.ALL, 5 )

        self.txt_username = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( self.txt_username, 1, wx.ALL|wx.EXPAND, 5 )

        self.lbl_password = wx.StaticText( self, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.lbl_password.Wrap( -1 )

        fgSizer1.Add( self.lbl_password, 0, wx.ALL, 5 )

        self.txt_password = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
        fgSizer1.Add( self.txt_password, 1, wx.ALL|wx.EXPAND, 5 )


        self.SetSizer( fgSizer1 )
        self.Layout()
        fgSizer1.Fit( self )

    def __del__( self ):
        pass


###########################################################################
## Class NewConnection
###########################################################################

class NewConnection ( wx.Dialog ):

    def __init__( self, parent ):
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"New Connection", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        bSizer9 = wx.BoxSizer( wx.VERTICAL )

        bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText9 = wx.StaticText( self, wx.ID_ANY, u"Name:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )

        bSizer10.Add( self.m_staticText9, 0, wx.ALL, 5 )

        self.txt_connection_name = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.txt_connection_name.SetMinSize( wx.Size( 150,-1 ) )

        bSizer10.Add( self.txt_connection_name, 0, wx.ALL, 5 )


        bSizer9.Add( bSizer10, 1, wx.ALL|wx.EXPAND, 5 )

        m_sdbSizer1 = wx.StdDialogButtonSizer()
        self.m_sdbSizer1OK = wx.Button( self, wx.ID_OK )
        m_sdbSizer1.AddButton( self.m_sdbSizer1OK )
        self.m_sdbSizer1Cancel = wx.Button( self, wx.ID_CANCEL )
        m_sdbSizer1.AddButton( self.m_sdbSizer1Cancel )
        m_sdbSizer1.Realize();

        bSizer9.Add( m_sdbSizer1, 1, wx.EXPAND, 5 )


        self.SetSizer( bSizer9 )
        self.Layout()
        bSizer9.Fit( self )

        self.Centre( wx.BOTH )

        # Connect Events
        self.Bind( wx.EVT_CLOSE, self.close )
        self.m_sdbSizer1OK.Bind( wx.EVT_BUTTON, self.add_connection )

    def __del__( self ):
        pass


    # Virtual event handlers, overide them in your derived class
    def close( self, event ):
        event.Skip()

    def add_connection( self, event ):
        event.Skip()


###########################################################################
## Class About
###########################################################################

class About ( wx.Dialog ):

    def __init__( self, parent ):
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"About", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_DIALOG_STYLE )

        self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

        bSizer6 = wx.BoxSizer( wx.VERTICAL )

        bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"Simple SSH", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText5.Wrap( -1 )

        self.m_staticText5.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )
        self.m_staticText5.SetMinSize( wx.Size( 90,-1 ) )

        bSizer10.Add( self.m_staticText5, 0, wx.ALIGN_CENTER|wx.ALL, 5 )

        self.m_staticText9 = wx.StaticText( self, wx.ID_ANY, u"v0.1", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )

        bSizer10.Add( self.m_staticText9, 0, wx.ALIGN_CENTER|wx.LEFT, 5 )


        bSizer6.Add( bSizer10, 1, wx.EXPAND, 5 )

        self.m_staticText8 = wx.StaticText( self, wx.ID_ANY, u"Developed by Arne Weiß", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText8.Wrap( -1 )

        self.m_staticText8.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_ITALIC, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

        bSizer6.Add( self.m_staticText8, 1, wx.BOTTOM|wx.EXPAND|wx.LEFT, 5 )

        bSizer8 = wx.BoxSizer( wx.HORIZONTAL )

        self.m_staticText7 = wx.StaticText( self, wx.ID_ANY, u"Website:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7.Wrap( -1 )

        bSizer8.Add( self.m_staticText7, 0, wx.ALL, 5 )

        self.m_hyperlink1 = wx.adv.HyperlinkCtrl( self, wx.ID_ANY, u"simplessh.arne-weiss.de", u"https://simplessh.arne-weiss.de", wx.DefaultPosition, wx.DefaultSize, wx.adv.HL_DEFAULT_STYLE )
        bSizer8.Add( self.m_hyperlink1, 0, wx.ALL, 5 )


        bSizer6.Add( bSizer8, 0, wx.EXPAND, 5 )


        self.SetSizer( bSizer6 )
        self.Layout()
        bSizer6.Fit( self )

        self.Centre( wx.BOTH )

    def __del__( self ):
        pass
