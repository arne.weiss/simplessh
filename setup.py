from setuptools import setup, find_packages

setup(
    name='simplessh',
    version='0.1',
    description='',
    author='Arne Weiß',
    author_email='arne.weiss@udo.edu',
    url='simplessh.arne-weiss.de',
    packages=find_packages(), # ['simplessh', 'appdirs'],
    install_requires=[
        'wxPython',
        'appdirs',
    ],
    scripts=['simplessh'],
    data_files=[('simple_ssh', ['simple_ssh/minus.png', 'simple_ssh/plus.png'])],
)
