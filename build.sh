#!/usr/bin/env bash

rm *.deb

fpm -s python -t deb --python-bin python3 --python-package-name-prefix python3 \
	--python-disable-dependency wxPython --depends python3-wxgtk4.0 \
	--no-python-fix-name \
	./setup.py
